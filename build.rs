extern crate bindgen;

use std::env;
use std::path::PathBuf;

fn main() {
    let bindings = bindgen::Builder::default()
        .header("include/dlt.h")
        .allowlist_function("dlt_register_app")
        .allowlist_function("dlt_register_context")
        .allowlist_function("dlt_log_string")
        .allowlist_function("dlt_unregister_context")
        .allowlist_function("dlt_unregister_app")
        .allowlist_type("DltContext")
        .allowlist_type("DltLogLevelType")
        .rustified_enum(".*") // Rustify all enums 
        .derive_default(true) // Derive the Default trait for all structs
        .generate()
        .expect("Unable to generate bindings");

    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");
    println!("cargo:rustc-link-lib=dylib=dlt");
}
