#![allow(warnings)]
// Include the generated FFI bindings
include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

unsafe impl Sync for DltContext {}
unsafe impl Send for DltContext {}

pub struct DltLogger {
    logger: DltContext,
}



impl DltLogger {
    pub fn new(app_id: &str, context_id: &str) -> Self {
        let mut logger = DltContext::default();
        unsafe {
            dlt_register_app(app_id.as_ptr() as *const _, context_id.as_ptr() as *const _);
            dlt_register_context(&mut logger, context_id.as_ptr() as *const _, app_id.as_ptr() as *const _);
        }
        DltLogger { logger }
    }

    pub fn log(&mut self, lvl: DltLogLevelType, message: &str) {
        unsafe {
            dlt_log_string(&mut self.logger as *mut _, lvl, message.as_ptr() as *const _);
        }
    }
}

impl Drop for DltLogger {
    fn drop(&mut self) {
        unsafe {
            dlt_unregister_context(&mut self.logger);
            dlt_unregister_app();
        }
    }
}

#[macro_export]
macro_rules! dlt_println {
    ($logger:expr, $lvl:expr, $msg:expr) => {{
        let null_terminated_msg = format!("{}\0", $msg);
        $logger.log($lvl, &null_terminated_msg);
    }};
    ($logger:expr, $lvl:expr, $($arg:tt)*) => {{
        let null_terminated_msg = format!("{}\0", format!($($arg)*));
        $logger.log($lvl, &null_terminated_msg);
    }};
}



#[macro_export]
macro_rules! dlt_err {
    ($logger:expr, $($arg:tt)*) => {{
        dlt_println!($logger, DltLogLevelType::DLT_LOG_ERROR, $($arg)*);
    }};
}

#[macro_export]
macro_rules! dlt_warn {
    ($logger:expr, $($arg:tt)*) => {{
        dlt_println!($logger, DltLogLevelType::DLT_LOG_WARN, $($arg)*);
    }};
}

#[macro_export]
macro_rules! dlt_info {
    ($logger:expr, $($arg:tt)*) => {{
        dlt_println!($logger, DltLogLevelType::DLT_LOG_INFO, $($arg)*);
    }};
}

#[macro_export]
macro_rules! dlt_dbg {
    ($logger:expr, $($arg:tt)*) => {{
        dlt_println!($logger, DltLogLevelType::DLT_LOG_DEBUG, $($arg)*);
    }};
}

#[macro_export]
macro_rules! dlt_verbose {
    ($logger:expr, $($arg:tt)*) => {{
        dlt_println!($logger, DltLogLevelType::DLT_LOG_VERBOSE, $($arg)*);
    }};
}

#[macro_export]
macro_rules! dlt_fatal {
    ($logger:expr, $($arg:tt)*) => {{
        dlt_println!($logger, DltLogLevelType::DLT_LOG_FATAL, $($arg)*);
    }};
}

